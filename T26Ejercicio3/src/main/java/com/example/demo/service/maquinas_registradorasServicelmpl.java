package com.example.demo.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.demo.dao.maquinas_registradorasDAO;
import com.example.demo.dto.maquinas_registradoras;
@Service
public class maquinas_registradorasServicelmpl implements maquinas_registradorasService {
	
	@Autowired
	maquinas_registradorasDAO maquinas_registradorasDAO;

	@Override
	public List<maquinas_registradoras> listarmaquinas_registradoras() {
		return maquinas_registradorasDAO.findAll();
	}

	@Override
	public maquinas_registradoras guardarmaquinas_registradoras(maquinas_registradoras maquinas_registradoras) {
		return maquinas_registradorasDAO.save(maquinas_registradoras);
	}

	@Override
	public maquinas_registradoras maquinas_registradorasXID(int id) {
		return maquinas_registradorasDAO.findById(id).get();
	}

	@Override
	public maquinas_registradoras actualizarmaquinas_registradoras(maquinas_registradoras maquinas_registradoras) {
		return maquinas_registradorasDAO.save(maquinas_registradoras);
	}

	@Override
	public void eliminarmaquinas_registradoras(int id) {
		maquinas_registradorasDAO.deleteById(id);
	}

}

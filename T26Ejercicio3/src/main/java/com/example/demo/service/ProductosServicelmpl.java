package com.example.demo.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.demo.dao.ProductosDAO;
import com.example.demo.dto.Productos;
@Service
public class ProductosServicelmpl implements ProductosService {

	@Autowired
	ProductosDAO productosDAO;

	@Override
	public List<Productos> listarProductos() {
		return productosDAO.findAll();
	}

	@Override
	public Productos guardarProductos(Productos Productos) {
		return productosDAO.save(Productos);
	}

	@Override
	public Productos ProductosXID(int id) {
		return productosDAO.findById(id).get();
	}

	@Override
	public Productos actualizarProductos(Productos Productos) {
		return productosDAO.save(Productos);
	}

	@Override
	public void eliminarProductos(int id) {
		productosDAO.deleteById(id);
	}
	
}

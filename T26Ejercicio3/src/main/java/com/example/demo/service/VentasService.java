package com.example.demo.service;

import java.util.List;

import com.example.demo.dto.Ventas;

public interface VentasService {
	
	public List<Ventas> listarVentas();
	
	public Ventas guardarVentas(Ventas ventas);
	
	public Ventas VentasXID(int id);
	
	public Ventas actualizarVentas(Ventas ventas);
	
	public void eliminarVentas(int id);

}

package com.example.demo.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.demo.dao.CajerosDAO;
import com.example.demo.dto.Cajeros;

@Service
public class CajerosServicelmpl implements CajerosService {
	
	@Autowired
	CajerosDAO cajerosDAO;

	@Override
	public List<Cajeros> listarCajeros() {
		return cajerosDAO.findAll();
	}

	@Override
	public Cajeros guardarCajeros(Cajeros cajeros) {
		return cajerosDAO.save(cajeros);
	}

	@Override
	public Cajeros cajerosXID(int id) {
		return cajerosDAO.findById(id).get();
	}

	@Override
	public Cajeros actualizarCajeros(Cajeros cajeros) {
		return cajerosDAO.save(cajeros);
	}

	@Override
	public void eliminarCajeros(int id) {
		cajerosDAO.deleteById(id);
	}


}

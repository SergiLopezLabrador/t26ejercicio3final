package com.example.demo.service;

import java.util.List;

import com.example.demo.dto.maquinas_registradoras;

public interface maquinas_registradorasService {
	
	public List<maquinas_registradoras> listarmaquinas_registradoras();
	
	public maquinas_registradoras guardarmaquinas_registradoras(maquinas_registradoras maquinas_registradoras);
	
	public maquinas_registradoras maquinas_registradorasXID(int id);
	
	public maquinas_registradoras actualizarmaquinas_registradoras(maquinas_registradoras maquinas_registradoras);
	
	public void eliminarmaquinas_registradoras(int id);

}

package com.example.demo.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.demo.dao.VentasDAO;
import com.example.demo.dto.Ventas;
@Service
public class VentasServicelmpl implements VentasService{
	
	@Autowired
	VentasDAO ventasDAO;

	@Override
	public List<Ventas> listarVentas() {
		return ventasDAO.findAll();
	}

	@Override
	public Ventas guardarVentas(Ventas ventas) {
		return ventasDAO.save(ventas);
	}

	@Override
	public Ventas VentasXID(int id) {
		return ventasDAO.findById(id).get();
	}

	@Override
	public Ventas actualizarVentas(Ventas ventas) {
		return ventasDAO.save(ventas);
	}

	@Override
	public void eliminarVentas(int id) {
		ventasDAO.deleteById(id);
	}

}

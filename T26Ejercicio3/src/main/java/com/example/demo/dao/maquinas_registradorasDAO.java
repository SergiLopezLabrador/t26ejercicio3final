package com.example.demo.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import com.example.demo.dto.maquinas_registradoras;

public interface maquinas_registradorasDAO extends JpaRepository<maquinas_registradoras, Integer>{

}

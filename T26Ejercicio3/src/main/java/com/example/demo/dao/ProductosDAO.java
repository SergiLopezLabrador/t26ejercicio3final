package com.example.demo.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import com.example.demo.dto.Productos;

public interface ProductosDAO extends JpaRepository<Productos, Integer> {

}

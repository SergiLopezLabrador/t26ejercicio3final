package com.example.demo.dto;


import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name="venta")
public class Ventas {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;

	@ManyToOne
    @JoinColumn(name = "cajero_id")
    Cajeros cajeros;
 
    @ManyToOne
    @JoinColumn(name = "maquina_id")
    maquinas_registradoras maquinas_registradoras;
    
    @ManyToOne
    @JoinColumn(name = "producto_id")
    Productos productos;

    public Ventas() {
    	
	}

	public Ventas(int id, Cajeros cajeros, maquinas_registradoras maquinas_registradoras, Productos productos) {
		this.id = id;
		this.cajeros = cajeros;
		this.maquinas_registradoras = maquinas_registradoras;
		this.productos = productos;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public Cajeros getCajeros() {
		return cajeros;
	}

	public void setCajeros(Cajeros cajeros) {
		this.cajeros = cajeros;
	}

	public maquinas_registradoras getMaquinas_registradoras() {
		return maquinas_registradoras;
	}

	public void setMaquinas_registradoras(maquinas_registradoras maquinas_registradoras) {
		this.maquinas_registradoras = maquinas_registradoras;
	}

	public Productos getProductos() {
		return productos;
	}

	public void setProductos(Productos productos) {
		this.productos = productos;
	}

	@Override
	public String toString() {
		return "Ventas [id=" + id + ", cajeros=" + cajeros + ", maquinas_registradoras=" + maquinas_registradoras
				+ ", productos=" + productos + "]";
	}
	

}

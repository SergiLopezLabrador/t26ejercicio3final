package com.example.demo.dto;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name="cajeros")
public class Cajeros {

	
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;
	@Column(name = "nomapels")
	private String nomapels;
	
	@OneToMany
    @JoinColumn(name="cajero_id")
    private List<Ventas> ventas;

	public Cajeros() {
		
	}

	public Cajeros(int id, String nomapels,  List<Ventas> ventas) {
		super();
		this.id = id;
		this.nomapels = nomapels;
		this.ventas = ventas;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getNomapels() {
		return nomapels;
	}

	public void setNomapels(String nomapels) {
		this.nomapels = nomapels;
	}


	@Override
	public String toString() {
		return "Cajeros [id=" + id + ", nomapels=" + nomapels + "]";
	}
}

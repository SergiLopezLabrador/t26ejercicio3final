package com.example.demo.dto;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name="productos")
public class Productos {


		@Id
		@GeneratedValue(strategy = GenerationType.IDENTITY)
		private int id;
		@Column(name = "nombre")
		private String nombre;
		@Column(name = "precio")
		private Integer precio;

		@OneToMany
	    @JoinColumn(name="id")
	    private List<Ventas> ventas;

		public Productos() {
			
		}

		public Productos(int id, String nombre,  Integer precio, List<Ventas> ventas) {
			super();
			this.id = id;
			this.nombre = nombre;
			this.precio = precio;
			this.ventas = ventas;
		}

		public int getId() {
			return id;
		}

		public void setId(int id) {
			this.id = id;
		}

		public String getNombre() {
			return nombre;
		}

		public void setNombre(String nombre) {
			this.nombre = nombre;
		}


		public Integer getPrecio() {
			return precio;
		}

		public void setPrecio(Integer precio) {
			this.precio = precio;
		}


		@Override
		public String toString() {
			return "Productos [id=" + id + ", nombre=" + nombre + ", precio=" + precio + ", ventas=" + ventas + "]";
		}

}

package com.example.demo.dto;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import java.util.List;

@Entity
@Table(name="maquinas_registradoras")
public class maquinas_registradoras {

		@Id
		@GeneratedValue(strategy = GenerationType.IDENTITY)
		private int id;
		@Column(name = "piso")
		private Integer piso;

		@OneToMany
	    @JoinColumn(name="maquina_id")
	    private List<Ventas> ventas;
		
		public maquinas_registradoras() {
			
		}

		public maquinas_registradoras(int id, Integer piso,  List<Ventas> ventas) {
			super();
			this.id = id;
			this.piso = piso;
			this.ventas = ventas;
		}

		public int getId() {
			return id;
		}

		public void setId(int id) {
			this.id = id;
		}

		public Integer getPiso() {
			return piso;
		}

		public void setPiso(Integer piso) {
			this.piso = piso;
		}


		@Override
		public String toString() {
			return "maquinas_registradoras [id=" + id + ", piso=" + piso + "]";
		}

		

}

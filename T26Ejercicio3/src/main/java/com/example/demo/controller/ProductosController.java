package com.example.demo.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.dto.Productos;
import com.example.demo.service.ProductosServicelmpl;

@RestController
@RequestMapping("/api")
public class ProductosController {
	
	@Autowired
	ProductosServicelmpl productosServicelmpl;
	
	@GetMapping("/Productos")
	public List<Productos> listarCcursos(){
		return productosServicelmpl.listarProductos();
	}
	
	
	@PostMapping("/Productos")
	public Productos salvarProductos(@RequestBody Productos curso) {
		
		return productosServicelmpl.guardarProductos(curso);
	}
	
	
	@GetMapping("/Productos/{id}")
	public Productos ProductosXID(@PathVariable(name="id") int id) {
		
		Productos Productos_xid= new Productos();
		
		Productos_xid=productosServicelmpl.ProductosXID(id);
		
		System.out.println("Productos XID: "+Productos_xid);
		
		return Productos_xid;
	}
	
	@PutMapping("/Productos/{id}")
	public Productos actualizarProductos(@PathVariable(name="id")int id,@RequestBody Productos Productos) {
		
		Productos Productos_seleccionado= new Productos();
		Productos Productos_actualizado= new Productos();
		
		Productos_seleccionado= productosServicelmpl.ProductosXID(id);
		
		Productos_seleccionado.setNombre(Productos.getNombre());
		Productos_seleccionado.setPrecio(Productos.getPrecio());
		
		Productos_actualizado = productosServicelmpl.actualizarProductos(Productos_seleccionado);
		
		System.out.println("El Productos actualizado es: "+ Productos_actualizado);
		
		return Productos_actualizado;
	}
	
	@DeleteMapping("/Productos/{id}")
	public void eliminarProductos(@PathVariable(name="id")int id) {
		productosServicelmpl.eliminarProductos(id);
	}

}

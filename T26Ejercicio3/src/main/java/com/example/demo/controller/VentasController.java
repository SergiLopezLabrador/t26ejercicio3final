package com.example.demo.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.dto.Ventas;
import com.example.demo.service.VentasServicelmpl;

@RestController
@RequestMapping("/api")
public class VentasController {
	
	@Autowired
	VentasServicelmpl ventasServicelmpl;
	
	@GetMapping("/ventas")
	public List<Ventas> listarCcursos(){
		return ventasServicelmpl.listarVentas();
	}
	
	
	@PostMapping("/ventas")
	public Ventas salvarVentas(@RequestBody Ventas ventas) {
		
		return ventasServicelmpl.guardarVentas(ventas);
	}
	
	
	@GetMapping("/ventas/{id}")
	public Ventas VentasXID(@PathVariable(name="id") int id) {
		
		Ventas ventas_xid= new Ventas();
		
		ventas_xid=ventasServicelmpl.VentasXID(id);
		
		System.out.println("Ventas XID: "+ventas_xid);
		
		return ventas_xid;
	}
	
	@PutMapping("/ventas/{id}")
	public Ventas actualizarVentas(@PathVariable(name="id")int id,@RequestBody Ventas ventas) {
		
		Ventas ventas_seleccionado= new Ventas();
		Ventas ventas_actualizado= new Ventas();
		
		ventas_seleccionado= ventasServicelmpl.VentasXID(id);
		
		ventas_actualizado = ventasServicelmpl.actualizarVentas(ventas_seleccionado);
		
		System.out.println("El Ventas actualizado es: "+ ventas_actualizado);
		
		return ventas_seleccionado;
	}
	
	@DeleteMapping("/ventas/{id}")
	public void eliminarVentas(@PathVariable(name="id")int id) {
		ventasServicelmpl.eliminarVentas(id);
	}


}

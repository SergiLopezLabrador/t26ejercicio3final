package com.example.demo.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.dto.maquinas_registradoras;
import com.example.demo.service.maquinas_registradorasServicelmpl;

@RestController
@RequestMapping("/api")
public class maquinas_registradorasController {
	
	@Autowired
	maquinas_registradorasServicelmpl maquinas_registradorasServicelmpl;
	
	@GetMapping("/maquinas_registradoras")
	public List<maquinas_registradoras> listarmaquinas_registradoras(){
		return maquinas_registradorasServicelmpl.listarmaquinas_registradoras();
	}
	
	
	@PostMapping("/maquinas_registradoras")
	public maquinas_registradoras salvarmaquinas_registradoras(@RequestBody maquinas_registradoras maquinas_registradoras) {
		
		return maquinas_registradorasServicelmpl.guardarmaquinas_registradoras(maquinas_registradoras);
	}
	
	
	@GetMapping("/maquinas_registradoras/{id}")
	public maquinas_registradoras maquinas_registradorasXID(@PathVariable(name="id") int id) {
		
		maquinas_registradoras maquinas_registradoras_xid= new maquinas_registradoras();
		
		maquinas_registradoras_xid=maquinas_registradorasServicelmpl.maquinas_registradorasXID(id);
		
		System.out.println("maquinas_registradoras XID: "+maquinas_registradoras_xid);
		
		return maquinas_registradoras_xid;
	}
	
	@PutMapping("/maquinas_registradoras/{id}")
	public maquinas_registradoras actualizarmaquinas_registradoras(@PathVariable(name="id")int id,@RequestBody maquinas_registradoras maquinas_registradoras) {
		
		maquinas_registradoras maquinas_registradoras_seleccionado= new maquinas_registradoras();
		maquinas_registradoras maquinas_registradoras_actualizado= new maquinas_registradoras();
		
		maquinas_registradoras_seleccionado= maquinas_registradorasServicelmpl.maquinas_registradorasXID(id);
		
		maquinas_registradoras_seleccionado.setPiso(maquinas_registradoras.getPiso());
		
		maquinas_registradoras_actualizado = maquinas_registradorasServicelmpl.actualizarmaquinas_registradoras(maquinas_registradoras_seleccionado);
		
		System.out.println("El maquinas_registradoras actualizado es: "+ maquinas_registradoras_actualizado);
		
		return maquinas_registradoras_actualizado;
	}
	
	@DeleteMapping("/maquinas_registradoras/{id}")
	public void eliminarmaquinas_registradoras(@PathVariable(name="id")int id) {
		maquinas_registradorasServicelmpl.eliminarmaquinas_registradoras(id);
	}


}

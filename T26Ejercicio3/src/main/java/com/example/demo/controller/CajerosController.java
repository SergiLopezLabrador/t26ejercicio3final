package com.example.demo.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.dto.Cajeros;
import com.example.demo.service.CajerosServicelmpl;

@RestController
@RequestMapping("/api")
public class CajerosController {
	
	@Autowired
	CajerosServicelmpl cajerosServicelmpl;
	
	@GetMapping("/cajeros")
	public List<Cajeros> listarCajeros(){
		return cajerosServicelmpl.listarCajeros();
	}
	
	@PostMapping("/cajeros")
	public Cajeros salvarCajeros(@RequestBody Cajeros cajeros) {
		
		return cajerosServicelmpl.guardarCajeros(cajeros);
	}
	
	@GetMapping("/cajeros/{id}")
	public Cajeros cajerosXID(@PathVariable(name="id") int id) {
		
		Cajeros Cajeros_xid= new Cajeros();
		
		Cajeros_xid=cajerosServicelmpl.cajerosXID(id);
		
		System.out.println("Cajeros XID: "+Cajeros_xid);
		
		return Cajeros_xid;
	}

	@PutMapping("/cajeros/{id}")
	public Cajeros actualizarCajeros(@PathVariable(name="id")int id,@RequestBody Cajeros cajeros) {
		
		Cajeros Cajeros_seleccionado= new Cajeros();
		Cajeros Cajeros_actualizado= new Cajeros();
		
		Cajeros_seleccionado= cajerosServicelmpl.cajerosXID(id);
		
		Cajeros_seleccionado.setNomapels(cajeros.getNomapels());
		
		Cajeros_actualizado = cajerosServicelmpl.actualizarCajeros(Cajeros_seleccionado);
		
		System.out.println("El cajero actualizado es: "+ Cajeros_actualizado);
		
		return Cajeros_actualizado;
	}

	@DeleteMapping("/cajeros/{id}")
	public void eliminarCajeros(@PathVariable(name="id")int id) {
		cajerosServicelmpl.eliminarCajeros(id);
	}


}

DROP table IF EXISTS venta;
DROP table IF EXISTS productos;
DROP table IF EXISTS maquinas_registradoras;
DROP table IF EXISTS cajeros;

CREATE TABLE `cajeros` (
  `id` int NOT NULL AUTO_INCREMENT,
  `nomapels` varchar(250) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `maquinas_registradoras` (
  `id` int NOT NULL AUTO_INCREMENT,
  `piso` int DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `productos` (
  `id` int NOT NULL AUTO_INCREMENT,
  `nombre` varchar(250) DEFAULT NULL,
  `precio` int DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `venta` (
  `id` int NOT NULL AUTO_INCREMENT,
  `cajero_id` int DEFAULT NULL,
  `maquina_id` int DEFAULT NULL,
  `producto_id` int DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `cajero_id_idx` (`cajero_id`),
  KEY `maquina_id_idx` (`maquina_id`),
  KEY `producto_id_idx` (`producto_id`),
  CONSTRAINT `cajero_id` FOREIGN KEY (`cajero_id`) REFERENCES `cajeros` (`id`),
  CONSTRAINT `maquina_id` FOREIGN KEY (`maquina_id`) REFERENCES `maquinas_registradoras` (`id`),
  CONSTRAINT `producto_id` FOREIGN KEY (`producto_id`) REFERENCES `productos` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

insert into cajeros (nomapels)values('cajerolopez');
insert into maquinas_registradoras (piso)values(14);
insert into productos (nombre, precio)values('monster', 3);
insert into venta (cajero_id, maquina_id, producto_id)values(1, 1, 1);